﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAL_Exercise___Relationships_and_Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            // This exercise contains 4 Classes and 1 Interface. Create these as shown in the UML diagram
            // and try to implement the relationships as shown in the Seperation of Concerns powerpoint
            //
            // The Car realizes the interface IVehicle, and as such will also contain the methods Drive and Stop.
            // These methods wont return anything, they will just call the appropriate method from the Car's
            // field variables, and those methods will only print a message to the console showing that the
            // methods were called (i.e. "Slowing Down" and "Moving Forward").

            // After implementing the classes and interfaces, create a student and car (and ensure the proper relationship
            // is created) and call both Drive and Stop methods from the Car

            Console.ReadKey();
        }
    }
}
